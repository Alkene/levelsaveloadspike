﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Level : ScriptableObject {

    [SerializeField]
    private List<LevelPiecePosition> levelData = new List<LevelPiecePosition>(); //Should just store enums, then reference prefabs through the enums
    public List<LevelPiecePosition> LevelData { get { return levelData; } set { levelData = value; Debug.Log(levelData.Count); } }

    public List<GameObject> LoadedGameObjects = new List<GameObject>();

    public void LoadLevel()
    {
        if (LoadedGameObjects.Count != LevelData.Count)
        {
            foreach (LevelPiecePosition levelPiece in LevelData)
            {
                Debug.Log("something found");
                LoadedGameObjects.Add(levelPiece.InstantiateCube());
            }
        }
    }

    public void UnloadLevel()
    {
        foreach(GameObject gameObject in LoadedGameObjects)
        {
            GameObject.Destroy(gameObject);
        }
        LoadedGameObjects.Clear();
    }
}