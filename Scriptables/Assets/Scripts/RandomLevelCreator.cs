﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System.IO;

public class RandomLevelCreator : MonoBehaviour
{
    public int NumberOfCubesToSpawn;

    public HubHolder levelSaveObject;
    public List<GameObject> levelObjects; //A downside to this is that if the list changes order, the levels will be fked

    public void CreateRandomLevel()
    {
        List<LevelPiecePosition> RandomLevelPiecePositionList = new List<LevelPiecePosition>(CreateRandomLevelPiecePositionList());
        levelSaveObject.OverrideLevel(CreateLevelScriptable(RandomLevelPiecePositionList), 0, 0);
    }

    private List<LevelPiecePosition> CreateRandomLevelPiecePositionList()
    {        
        List<LevelPiecePosition> levelPiecePositionList = new List<LevelPiecePosition>();
        for (int i = 0; i < NumberOfCubesToSpawn; i++)
        {
            GameObject prefab = levelObjects[Random.Range(0, levelObjects.Count)];
            levelPiecePositionList.Add(new LevelPiecePosition(FirstPieceRotation.Forward, SecondPieceRotation.Forward, new Vector3Int(0, 0, i), prefab));
        }
        return levelPiecePositionList;
    }

    public Level CreateLevelScriptable(List<LevelPiecePosition> levelPiecePositions)
    {
        Level level = ScriptableObjectUtility.CreateAsset<Level>();
        level.LevelData = levelPiecePositions;;
        ScriptableObjectUtility.ForceSerialization(level); //Without this line, levels have a tendancy to lose their data randomly. This also means that we can only save scriptable objects in editor.

        return level;
    }
}
