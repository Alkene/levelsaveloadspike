﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Hub : ScriptableObject {
    public List<Level> levels;
}
