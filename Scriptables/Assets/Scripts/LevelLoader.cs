﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelLoader : MonoBehaviour {
    [SerializeField]
    HubHolder hubHolder;

    [SerializeField]
    Level loadedLevel;

    public void LoadLastLevel()
    {
        if (loadedLevel)
        {
            UnloadLastLevel();
        }
        hubHolder.GetLevel().LoadLevel();
        loadedLevel = hubHolder.GetLevel();
    }

    public void UnloadLastLevel()
    {
        if (loadedLevel)
        {
            loadedLevel.UnloadLevel();
            loadedLevel = null;
        }
    }
}
