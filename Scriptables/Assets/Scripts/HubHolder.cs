﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "LevelSaveLoad")]
public class HubHolder : ScriptableObject{

    public List<Hub> hubs = new List<Hub>();

    public void AddLevel(Level level)
    {
        if (hubs.Count > 0)
        {
            hubs[hubs.Count - 1].levels.Add(level);
            ScriptableObjectUtility.SaveAssets();
        }
        else
        {
            Debug.LogError("No hubs exist");
        }
    }

    public void AddLevel(Level level, int hub)
    {
        if (hubs.Count > hub)
        {
            hubs[hub].levels.Add(level);
            ScriptableObjectUtility.SaveAssets();
        }
        else
        {
            Debug.LogError("You're trying to access a hub that doesn't exist");
        }
    }

    public void AddLevel(Level level, int hub, int levelNumber)
    {
        if (hubs.Count < hub)
        {
            if (hubs[hubs.Count].levels.Count > levelNumber)
            {
                hubs[hub].levels[levelNumber] = level;
                ScriptableObjectUtility.SaveAssets();
            }
            else if (hubs[hubs.Count].levels.Count == levelNumber)
            {
                hubs[hub].levels.Add(level);
                ScriptableObjectUtility.SaveAssets();
            }
            else
            {
                Debug.LogError("You're trying to add/edit a level that doesn't exist");
            }
        }
        else
        {
            Debug.LogError("You're trying to access a hub that doesn't exist");
        }
        
    }

    public void OverrideLevel(Level level, int hub, int levelNumber)
    {
        hubs[hub].levels[levelNumber] = level;
        ScriptableObjectUtility.SaveAssets();
    }

    public Level GetLevel(int hub, int levelNumber) 
    {
        if (hubs.Count > hub &&
            hubs[hub].levels.Count > levelNumber)
        {
            return hubs[hub].levels[levelNumber];
        }
        else
        {
            Debug.LogError("Out of range level access");
            return null;
        }

    }

    public Level GetLevel(int levelNumber) 
    {
        if (hubs[hubs.Count - 1].levels.Count > levelNumber)
        {
            return hubs[hubs.Count - 1].levels[levelNumber];
        }
        else
        {
            Debug.LogError("Trying to access nonexistant level");
            return null;
        }
    }
    
    public Level GetLevel()
    {
        if (hubs.Count > 0 && hubs[hubs.Count - 1].levels.Count > 0)
        {
            return hubs[hubs.Count - 1].levels[hubs[hubs.Count - 1].levels.Count - 1];
        }
        else
        {
            Debug.LogError("You're trying to access a hub or level that doesn't exist");
            return null;
        }
    }
}
