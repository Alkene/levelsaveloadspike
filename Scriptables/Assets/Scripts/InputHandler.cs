﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputHandler : MonoBehaviour
{
    public RandomLevelCreator randomLevelCreator;
    public LevelLoader levelLoader;

    [SerializeField]
    private KeyCode createLevelKey = KeyCode.Q;
    [SerializeField]
    private KeyCode loadLevelKey = KeyCode.W;
    [SerializeField]
    private KeyCode unloadLevelKey = KeyCode.E;

    private void Update()
    {
        if (Input.GetKeyDown(createLevelKey))
        {
            randomLevelCreator.CreateRandomLevel();
        }

        if (Input.GetKeyDown(loadLevelKey))
        {
            levelLoader.LoadLastLevel();
        }

        if (Input.GetKeyDown(unloadLevelKey))
        {
            levelLoader.UnloadLastLevel(); 
        }
    }
}