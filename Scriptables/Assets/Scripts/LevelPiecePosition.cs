﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum FirstPieceRotation
{
    Up,
    Down,
    Left,
    Right, 
    Forward,
    Backward
}

public enum SecondPieceRotation
{
    Forward,
    Backward,
    Left,
    Right
}

[System.Serializable]
public class LevelPiecePosition {
    public FirstPieceRotation FirstPieceRotation;
    public SecondPieceRotation SecondPieceRotation;
    public Vector3Int Position;

    public GameObject Prefab;

    public LevelPiecePosition(){}

    public LevelPiecePosition (FirstPieceRotation firstPieceRot, SecondPieceRotation secondPieceRot, Vector3Int pos, GameObject prefab)
    {
        FirstPieceRotation = firstPieceRot;
        SecondPieceRotation = secondPieceRot;
        Position = pos;

        Prefab = prefab;
    }

    public GameObject InstantiateCube()
    {
        Debug.Log("attempting to instantiate a cube: " + Prefab.name + ", " + Position);
        return GameObject.Instantiate(Prefab, Position, Quaternion.identity);
    }
}
