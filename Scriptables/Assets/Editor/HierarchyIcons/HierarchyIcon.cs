﻿using UnityEditor;
using UnityEditor.SceneManagement;
using UnityEngine;
using System.Collections.Generic;

[InitializeOnLoad]
class HierarchyIcon 
{
    static Texture2D m_ActiveObjectTexture;
    static Texture2D m_HiddenActiveObjectTexture;
    static Texture2D m_InactiveObjectTexture;

    public static GUISkin m_GUISkin;

    public static string m_GenericPath = "Assets/Editor/HierarchyIcons/";

    public static string m_ActiveIconPath = "Active_Icon.png";
    public static string m_HiddenActiveIconPath = "Hidden_Active_Icon.png";
    public static string m_InactiveIconPath = "Inactive_Icon.png";
  
    public static string m_GUISkinPath = "Hierarchy_GUI_Skin.guiskin";

    public static int m_ButtonSize = 14;

    public static int m_IconAdjustmentSize = 4;
    public static int m_IconSize = 10;

    public static int m_DepthOffset = 14;

    public static int m_SearchOffset = 14;

    /// Constructor ///
    static HierarchyIcon()
    {
        // Load required assets
        m_ActiveObjectTexture = AssetDatabase.LoadAssetAtPath(m_GenericPath + m_ActiveIconPath, typeof(Texture2D)) as Texture2D;
        ErrorCheckLoadedAsset(m_ActiveObjectTexture, "Active Object Texture");
        m_HiddenActiveObjectTexture = AssetDatabase.LoadAssetAtPath(m_GenericPath + m_HiddenActiveIconPath, typeof(Texture2D)) as Texture2D;
        ErrorCheckLoadedAsset(m_HiddenActiveObjectTexture, "Hidden Active Object Texture");
        m_InactiveObjectTexture = AssetDatabase.LoadAssetAtPath(m_GenericPath + m_InactiveIconPath, typeof(Texture2D)) as Texture2D;
        ErrorCheckLoadedAsset(m_InactiveObjectTexture, "Inactive Object Texture");

        m_GUISkin = AssetDatabase.LoadAssetAtPath(m_GenericPath + m_GUISkinPath,typeof(GUISkin)) as GUISkin;
        ErrorCheckLoadedAsset(m_GUISkin, "GUI Skin");
		
        // Attach our callback function
		EditorApplication.hierarchyWindowItemOnGUI += HierarchyItemCB;
    }

    /// Ensures that an asset has loaded and prints a debug log error message if it hasn't ///
    private static void ErrorCheckLoadedAsset(Object _obj, string _name)
    {
        if (_obj == null)
        {
            Debug.LogError("Failed to initialize (" + _name + ").");
        }
    }

    /// Takes a transform and calculates its scene depth (how many parents it has) ///
    private static int CalculateTransformDepth(Transform _t)
    {
        for (int depth = 0; depth < 100; depth++)
        {
            if (_t.parent == null)
            {
                return depth;
            }
            _t = _t.parent;
        }
        return 0;
    }

    /// Callback attached to the hierarchy window OnGUI call ///
    private static void HierarchyItemCB(int _instanceID, Rect _selectionRect)
    {
        // Getting access to the gameobject we're about to draw icons for
        GameObject go = EditorUtility.InstanceIDToObject(_instanceID) as GameObject;
        if (go == null)
        {
            return;
        }

        // Initialize and modify the rect
        Rect activeIcon = new Rect();
        activeIcon.x = _selectionRect.x - 29;
		activeIcon.width = m_ButtonSize;
        activeIcon.height = m_ButtonSize;
        activeIcon.y = _selectionRect.y + 2;

        // We're searching in the hierarchy, so offset for that
        if (_selectionRect.x == 16)
        {
            activeIcon.x += 14;
        }

        // Draw the button and icons
        DrawActivateButton(activeIcon, 0, 1, go);
    }

    /// Draws the toggle button as well as the correct icon ///
    static void DrawActivateButton(Rect _rect, float _iconOffsetX, float _iconOffsetY, GameObject _go)
    {
        // Decide on the display icon
        Texture2D displayIcon = null;
        if (_go.activeInHierarchy) 
        {
            displayIcon = m_ActiveObjectTexture;
        }
        else if (_go.activeSelf)
        {
            displayIcon = m_HiddenActiveObjectTexture;
        }
        else
        {
            displayIcon = m_InactiveObjectTexture;
        }

        // Draw our custom toggle button - and evaluate for changes
        if (GUI.Toggle(_rect, _go.activeInHierarchy, "", m_GUISkin.GetStyle("button")) != _go.activeInHierarchy)
        {
            // Store the action on the undo stack for undo-ing/redo-ing
            Undo.RecordObject(_go, "Toggle Object: " + _go.name);
            _go.SetActive(!_go.activeSelf);

            // Mark the scene dirty so the change is save-able
            if (!Application.isPlaying)
            {
                EditorSceneManager.MarkSceneDirty(_go.scene);
            }
        }

        // Reshape the rect and then draw our icon to it
        _rect.x += _iconOffsetX;
        _rect.y += _iconOffsetY;
        _rect.width = m_IconSize + m_IconAdjustmentSize;
        _rect.height = m_IconSize + m_IconAdjustmentSize;
        
        GUI.Label(_rect, displayIcon);
    }
}